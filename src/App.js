// @flow

import React from 'react';
import logo from './logo.svg';
import './App.css';

class App extends React.Component<{}>{
  
square (num: number): number{
 return num * num;
}

render(){
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <button style={{backgroundColor:'red'}} onClick={()=>console.log(this.square(2))}>Button</button>
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blankw"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}
  
}

export default App;
