import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer'; // ES6
import App from '../App';


describe('Render App component', ()=>{
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
})

describe('Square the number', ()=>{
    it('Function Square(4) return 16 ', () => {
      const div = document.createElement('div');
      const wrapper = renderer.create(<App/>,div)
      const AppInstance = wrapper.getInstance();
      expect(AppInstance.square(4)).toBe(16);
  });
})

